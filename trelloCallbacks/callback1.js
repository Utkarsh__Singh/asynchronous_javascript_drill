/* 
	TODO:
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function callback(id,Boards) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			let result = Boards.find(ele => ele.id === id)
			if (result) {
				resolve(result)
			}
			else {
				reject(new Error("invalid id passed"))
			}
		},3000)
	})
}
module.exports =callback
// const Boards=[
//     {
//         "id": "mcu453ed",
//         "name": "Thanos",
//         "permissions": {
            
//         }
//     },
//     {
//         "id": "abc122dc",
//         "name": "Darkseid"
//     }
// ]


