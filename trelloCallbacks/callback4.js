/* 
TODO:

    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boards = require('./data/boards.json');
const callback2 = require('./callback2');
const callback3 = require('./callback3');


const task4 = () => {
    let info = new Promise((resolve, reject) => {
        setTimeout(async () => {
            //! getting object for name Thanos in boards.json
            let boardObj = boards.find(object => object.name === 'Thanos');
            //!getting id of Thanso
            let boardID = boardObj.id;
            //!calling function from callback to get list info for Thanos's id
            let listInfo = await callback2(boardID);
            let listObject = listInfo.find(object => object.name === 'Mind');
            //getting id for name mind
            let idMind = listObject.id;
            //calling callback function to get cards for idMind
            let cardsInfo = await callback3(idMind);
            //resolving final output
            resolve(cardsInfo);
        }, 3000);
    });
    return info;
}


module.exports = task4;
