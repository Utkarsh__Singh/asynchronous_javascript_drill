/*
    TODO:
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const CARD= require('./data/cards.json')
const cardsInfo = (listID) => {
    let info = new Promise((res, rej) => {
        setTimeout(() => {
            res(CARD[listID]);
        }, 2 * 1000);
    });
    return info;
}


module.exports = cardsInfo;