/*  
    TODO:
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boards = require('./data/boards.json')
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const task6 = () => {
    let info = new Promise((res, rej) => {
        setTimeout(async () => {
            //! getting obj for name Thanos in boards.json
            let boardObj = boards.find(obj => obj.name === 'Thanos');

            //!getting id for Thanos
            let boardID = boardObj.id;

            //!calling function from callback to get list info for Thanos's id
            let listInfo = await callback2(boardID);

            //getting ids for all lists for Thonos board
            let allListIds = listInfo.map(obj => obj.id);

            //calling callback function to get cards for all ids
            let allcardsInfo = allListIds.map(async (ids) => {
                let cardsData = await callback3(ids);
                return cardsData;
            });

            //!resolve all promises inside cardsInfo 
            const promise4all = Promise.all(allcardsInfo);

            //resolving final output
            res(promise4all);

        }, 2 * 1000);
    });
    return info;
}

module.exports = task6;
