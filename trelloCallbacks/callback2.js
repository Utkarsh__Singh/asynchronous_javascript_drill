/* 
	TODO:
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const LIST = require('./data/lists.json')

const listInfo = (boardID) => {
    let info = new Promise((res, rej) => {
        setTimeout(() => {
            res(LIST[boardID])
        }, 2 * 1000);
    });
    return info;
}

module.exports = listInfo;

// const id = 'abc122dc'
// console.log(

//     callback(id, LIST).then(res => {
//         console.log(res)
//     })
// // )