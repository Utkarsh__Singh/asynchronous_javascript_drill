/*
    Problem 1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
// create a directory to store the random JSON files
const fs = require("fs");
const path = require("path");
// function to add random files to the directory
function addRandoms(i) {
    const filePath = path.join(`${__dirname}/randomJSON`, `RANDOM${i}.json`)
    fs.writeFile(filePath, JSON.stringify(i), (err) => {
        if (err) throw err
    })
    console.log(`RANDOM${i}.json created successfully`)
    fs.unlink(filePath, (err) => {
        if (err) throw err;
    })
    console.log(`RANDOM${i}.json deleted successfully` )
}

function createDirectory() {
    const randomJSON = path.join(__dirname, "randomJSON");
    
    fs.mkdir(randomJSON, (err) => {
        if (err) throw err;
        else{console.log(`directory created`)}
    })
}

// create new random JSOn directory
createDirectory();

// lets create 5 files to hold JSON data
for (let i = 1; i < 6; i++) {
    addRandoms(i)
}
